import React, {Component} from 'react';
import {Bar, Line, Scatter} from 'react-chartjs-2';

class Chart extends Component{
  constructor(props){
    super(props);
    this.state = {
      chartData:{
        type: 'scatter',
        labels: [],
        datasets: [{label:'Scatter Dataset',data:[{x:10, y:20.1},{x:15, y:10.1}],
        backgroundColor:'rgba(255,0,0,0.6)'
      }]
      }
    }
  }
  render(){
    return(
      <div className="chart">
      <Scatter
        data={this.state.chartData}
        options={{
          maintainAspectRatio: false
        }}
      />
      </div>
    )
  }
}
export default Chart;
